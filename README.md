# WSANLab

## Contains all programs of CCE WSAN Lab Manual 2020

In these trying times, labs were conducted **in Manipal** in the **middle of a pandemic** with *reduced syllabus*.
All programs and supporting documentation are provided **free of cost** and the owner is **not responsible for any kind of misunderstanding** that may arise due to the use of this code.
Queries will not be entertained. *Please refrain from misuse*.
Support the Open Source community of Manipal.
Create a pull request to contribute.

